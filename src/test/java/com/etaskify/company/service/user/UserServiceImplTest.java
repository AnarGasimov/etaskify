package com.etaskify.company.service.user;


import com.etaskify.company.dto.UserDTO;
import com.etaskify.company.exception.BadRequestException;
import com.etaskify.company.mapper.UserMapper;
import com.etaskify.company.model.Organization;
import com.etaskify.company.model.Role;
import com.etaskify.company.model.User;
import com.etaskify.company.payload.UserPayload;
import com.etaskify.company.repository.UserRepository;
import com.etaskify.company.service.OrganizationService;
import com.etaskify.company.service.UserService;
import com.etaskify.company.tool.EmailChecker;
import com.etaskify.company.tool.UsernameChecker;
import org.junit.Assert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.ArrayList;
import java.util.List;


@ExtendWith(SpringExtension.class)
@SpringBootTest
class UserServiceImplTest {

    @Autowired
    private UserService userService;

    @MockBean
    private UserRepository userRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @MockBean
    private OrganizationService organizationService;

    @MockBean
    private UsernameChecker usernameChecker;

    @MockBean
    private EmailChecker emailChecker;

    private Organization organization = new Organization();
    private UserPayload userPayload = new UserPayload();
    private List<User> users = new ArrayList<>();

    @BeforeEach
    void setUp() {
        organization = new Organization();
        organization.setId(1L);
        organization.setName("Etaskify");
        organization.setPhoneNumber("+99422222222");
        organization.setAddress(" address");

        userPayload.setEmail("etaskify@gmail.com");
        userPayload.setPassword("123456");
        userPayload.setName("user");
        userPayload.setUserName("user surname");
        userPayload.setUserName("useruser");


        User user1 = new User();
        user1.setId(1L);
        user1.setName("User 1 name");
        user1.setSurname("User 1 surname");
        user1.setOrganization(organization);

        User user2 = new User();
        user2.setId(2L);
        user2.setName("User 2 name");
        user2.setSurname("User 2 surname");
        user2.setOrganization(organization);

        users.add(user1);
        users.add(user2);
    }

    @Test
    void create_success() {
        Mockito.doNothing().when(emailChecker).check(userPayload.getEmail());
        Mockito.doNothing().when(usernameChecker).check(userPayload.getUserName());
        Mockito.doReturn(organization).when(organizationService).getOrganization();
        Mockito.doReturn(UserMapper.mapUserFromUserCreationPayload(userPayload))
                .when(userRepository).save(ArgumentMatchers.any());

        User user = userService.create(userPayload);

        Assert.assertNotNull(user);
        Assert.assertEquals(userPayload.getEmail(), user.getEmail());
        Assert.assertEquals(userPayload.getUserName(), user.getUsername());
        Assert.assertEquals(userPayload.getName(), user.getName());
        Assert.assertEquals(userPayload.getSurname(), user.getSurname());
        Assert.assertEquals(organization, user.getOrganization());
    }
    @Test
    void usernameIsAlreadyUsed() {
        Mockito.doThrow(BadRequestException.class).when(usernameChecker).check(userPayload.getUserName());
        Assert.assertThrows(BadRequestException.class, () -> userService.create(userPayload));
    }

    @Test
    void emailIsAlreadyUsed() {
        Mockito.doThrow(BadRequestException.class).when(emailChecker).check(ArgumentMatchers.anyString());
        Assert.assertThrows(BadRequestException.class, () -> userService.create(userPayload));
    }


    @Test
    void getUsersOfOrganization() {
        Mockito.doReturn(organization).when(organizationService).getOrganization();
        Mockito.doReturn(users).when(userRepository).findAllByOrganizationAndRoleIs(organization, Role.ROLE_USER);

        List<UserDTO> myOrganizationUsers = userService.getUsersOfOrganization();
        Assert.assertEquals(2, myOrganizationUsers.size());
    }
}
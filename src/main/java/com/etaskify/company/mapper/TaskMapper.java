package com.etaskify.company.mapper;


import com.etaskify.company.model.Task;
import com.etaskify.company.model.TaskStatus;
import com.etaskify.company.payload.TaskPayload;

public class TaskMapper {

    public static Task mapFromPayload(TaskPayload payload) {
        return new Task(
                payload.getTitle(),
                payload.getDescription(),
                payload.getDeadline(),
                TaskStatus.NEW
        );
    }

}

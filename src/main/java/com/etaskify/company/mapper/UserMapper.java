package com.etaskify.company.mapper;

import com.etaskify.company.model.Role;
import com.etaskify.company.model.User;
import com.etaskify.company.payload.RegistrationFormPayload;
import com.etaskify.company.payload.UserPayload;

public class UserMapper {

    public static User mapUserFromRegistrationPayload(RegistrationFormPayload payload) {
        return new User(
                payload.getUserName(),
                payload.getEmail(),
                Role.ROLE_ADMIN
        );
    }

    public static User mapUserFromUserCreationPayload(UserPayload payload) {
        return new User( payload.getEmail(),
                payload.getUserName(),
                payload.getName(),
                payload.getSurname(),
                Role.ROLE_USER
        );
    }

}

package com.etaskify.company.mapper;


import com.etaskify.company.dto.EmailMessage;
import com.etaskify.company.model.Task;

public class EmailMessageMapper {

    public static EmailMessage createForNotifyingNewTask(String email, Task task) {
        EmailMessage emailMessage = new EmailMessage();
        emailMessage.setEmail(email);
        emailMessage.setSubject("You are assigned to new task");
        emailMessage.setText(
                String.format("Task id: %d, title: %s, description: %s",
                        task.getId(), task.getTitle(), task.getDescription()));
        return emailMessage;
    }

}

package com.etaskify.company.mapper;


import com.etaskify.company.dto.UserDTO;
import com.etaskify.company.model.User;

public class UserDTOMapper {

    public static UserDTO mapFromUser(User user) {
        return new UserDTO(
                user.getId(),
                user.getName(),
                user.getSurname()
        );
    }

}

package com.etaskify.company.mapper;

import com.etaskify.company.dto.TaskDTO;
import com.etaskify.company.model.Task;

public class TaskDTOMapper {

    public static TaskDTO mapFromTask(Task task) {
        return new TaskDTO(
                task.getId(),
                task.getTitle(),
                task.getDescription(),
                task.getDeadline(),
                task.getStatus()
        );
    }

}

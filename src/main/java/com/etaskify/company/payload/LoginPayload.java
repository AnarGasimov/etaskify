package com.etaskify.company.payload;

import lombok.Data;

import javax.validation.constraints.NotBlank;


@Data
public class LoginPayload {
    @NotBlank(message = "Username or email should not be empty")
    private String usernameOrEmail;

    private String password;
}

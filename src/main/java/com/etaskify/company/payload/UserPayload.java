package com.etaskify.company.payload;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Data
public class UserPayload {

    @NotBlank(message = "Email should not be empty")
    private String email;

    @NotBlank(message = "Username should not be empty")
    private String userName;

    @NotBlank(message = "Password should not be empty")
    @Size(min = 6, message = "Password must contain at least 6 characters")
    private String password;

    @NotBlank(message = "Name should not be empty")
    private String name;

    @NotBlank(message = "Surname should not be empty")
    private String surname;

}

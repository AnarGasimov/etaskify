package com.etaskify.company.payload;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

@Data
public class RegistrationFormPayload {

    private static final String EMAIL_REGEX = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

    @Pattern(regexp = EMAIL_REGEX)
    private String email;

    @NotBlank(message = "Password must be at least 6 characters")
    private String password;

    @NotBlank(message = "Name should not be empty")
    private String name;

    @NotBlank(message = "Username should not be empty")
    private String userName;

    @NotBlank(message = "Organization should not be empty")
    private String organization;

    @NotBlank(message = "Phone number should not be empty")
    private String phoneNumber;

    @NotBlank(message = "Address should not be empty")
    private String address;

}

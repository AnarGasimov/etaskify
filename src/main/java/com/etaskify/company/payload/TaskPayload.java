package com.etaskify.company.payload;

import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.List;

@Data
public class TaskPayload {
    @NotBlank(message = "Title should not be empty")
    private String title;
    @NotBlank(message = "Description should not be empty")
    private String description;

    @NotNull(message = "Please enter deadline")
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private LocalDateTime deadline;

    private List<Long> userIds;
}

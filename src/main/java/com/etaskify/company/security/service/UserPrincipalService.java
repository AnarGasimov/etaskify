package com.etaskify.company.security.service;


import com.etaskify.company.model.User;
import com.etaskify.company.repository.UserRepository;
import com.etaskify.company.security.JwtUser;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class UserPrincipalService {

    private final UserRepository userRepository;

    @Autowired
    public UserPrincipalService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public User getUser() {
        log.info("Getting user from user principal");
        JwtUser jwtUser = (JwtUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return userRepository.findById(jwtUser.getId()).get();
    }

}

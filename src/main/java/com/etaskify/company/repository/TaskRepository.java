package com.etaskify.company.repository;


import com.etaskify.company.model.Organization;
import com.etaskify.company.model.Task;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TaskRepository extends JpaRepository<Task, Long> {
    List<Task> findAllByOrganization(Organization organization);
}

package com.etaskify.company.repository;

import com.etaskify.company.model.Organization;
import com.etaskify.company.model.Role;
import com.etaskify.company.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;


public interface UserRepository extends JpaRepository<User,Long> {
    Optional<User> findByUsernameOrEmail(String username, String email);
    Boolean existsByUsername(String username);
    Boolean existsByEmail(String email);
    List<User> findAllByOrganizationAndRoleIs(Organization organization, Role role);
}

package com.etaskify.company.controller;


import com.etaskify.company.payload.UserPayload;
import com.etaskify.company.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/users")
@PreAuthorize("hasRole('ADMIN')")
public class UserController {

    private UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping
    public ResponseEntity<?> getUsersOfOrganization() {
        return ResponseEntity.ok(userService.getUsersOfOrganization());
    }

    @PostMapping
    public ResponseEntity<?> create(@Valid @RequestBody UserPayload userPayload) {
        userService.create(userPayload);
        return ResponseEntity.ok("User is created!");
    }

}

package com.etaskify.company.controller;

import com.etaskify.company.payload.LoginPayload;
import com.etaskify.company.payload.RegistrationFormPayload;
import com.etaskify.company.service.AuthService;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/auth")
public class RegistrationAuthController {

    private final AuthService authService;

    public RegistrationAuthController(AuthService authService) {
        this.authService = authService;
    }

    @PostMapping("/register")
    public ResponseEntity<?> create(@Valid @RequestBody RegistrationFormPayload registrationFormPayload){
         authService.register(registrationFormPayload);

         return ResponseEntity.ok("Registration is completed");
    }

    @PostMapping("/login")
    public ResponseEntity<?> login(@Valid @RequestBody LoginPayload payload) {

        String token = authService.login(payload);
        HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.add("Authorization", token);

        return ResponseEntity.ok()
                .headers(responseHeaders)
                .body("Welcome!");
    }
}

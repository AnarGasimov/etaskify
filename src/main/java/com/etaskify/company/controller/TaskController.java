package com.etaskify.company.controller;


import com.etaskify.company.payload.TaskPayload;
import com.etaskify.company.service.TaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/tasks")
public class TaskController {

    private final TaskService taskService;

    @Autowired
    public TaskController(TaskService taskService) {
        this.taskService = taskService;
    }

    @PostMapping
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<?> create(@Valid @RequestBody TaskPayload taskPayload) {
        System.out.println("taskpayload" + taskPayload);
        return ResponseEntity.ok(taskService.create(taskPayload));
    }

    @GetMapping
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<?> getMyOrganizationTasks() {
        return ResponseEntity.ok(taskService.getOrganizationTasks());
    }

    @GetMapping("/usertasks")
    @PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
    public ResponseEntity<?> getTasks() {
        return ResponseEntity.ok(taskService.getMyTasks());
    }

}
package com.etaskify.company.tool;


import com.etaskify.company.exception.UserNotFoundException;
import com.etaskify.company.repository.UserRepository;
import lombok.AllArgsConstructor;

import org.springframework.stereotype.Component;

import java.util.List;

@Component
@AllArgsConstructor
public class UsersChecker {

    private final UserRepository userRepository;

    public void check(List<Long> userIds) {
        for (long userId : userIds) {
            if (!userRepository.existsById(userId)) {
                throw new UserNotFoundException("User  with id" + userId + " not found");
            }
        }
    }

}

package com.etaskify.company.tool;

import com.etaskify.company.exception.BadRequestException;
import com.etaskify.company.repository.UserRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Component
@AllArgsConstructor
@Slf4j
public class UsernameChecker {

    private final UserRepository userRepository;

    public void check(String userName) {
        if (userRepository.existsByUsername(userName)) {
            log.info("user with username {} is already registered ", userName);
            throw new BadRequestException("User is already registered");
        }
    }
}

package com.etaskify.company.tool;

import com.etaskify.company.exception.BadRequestException;
import com.etaskify.company.repository.UserRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class EmailChecker {

    private final UserRepository userRepository;

    public EmailChecker(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public void check(String email) {

        if (userRepository.existsByEmail(email)) {
            log.info("User email is already registered: {}", email);
            throw new BadRequestException("User email is already registered");
        }

    }
}

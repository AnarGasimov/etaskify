package com.etaskify.company.model;

public enum TaskStatus {
    NEW, IN_PROGRESS, DONE
}

package com.etaskify.company.service;


import com.etaskify.company.dto.EmailMessage;

public interface EmailService {
    void send(EmailMessage emailMessage);
}

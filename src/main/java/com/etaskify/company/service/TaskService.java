package com.etaskify.company.service;



import com.etaskify.company.dto.TaskDTO;
import com.etaskify.company.payload.TaskPayload;

import java.util.List;

public interface TaskService {
    TaskDTO create(TaskPayload taskPayload);
    List<TaskDTO> getMyTasks();
    List<TaskDTO> getOrganizationTasks();
}
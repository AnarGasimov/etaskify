package com.etaskify.company.service;


import com.etaskify.company.payload.LoginPayload;
import com.etaskify.company.payload.RegistrationFormPayload;

public interface AuthService {
    void register(RegistrationFormPayload payload);
    String login(LoginPayload payload);
}

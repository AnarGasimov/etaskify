package com.etaskify.company.service.imp;


import com.etaskify.company.dto.EmailMessage;
import com.etaskify.company.dto.UserDTO;
import com.etaskify.company.mapper.EmailMessageMapper;
import com.etaskify.company.mapper.UserDTOMapper;
import com.etaskify.company.mapper.UserMapper;
import com.etaskify.company.model.Role;
import com.etaskify.company.model.Task;
import com.etaskify.company.model.User;
import com.etaskify.company.payload.UserPayload;
import com.etaskify.company.repository.UserRepository;
import com.etaskify.company.service.EmailService;
import com.etaskify.company.service.OrganizationService;
import com.etaskify.company.service.UserService;
import com.etaskify.company.tool.EmailChecker;
import com.etaskify.company.tool.UsernameChecker;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;


@Service
@Slf4j
@AllArgsConstructor
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;
    private final OrganizationService organizationService;
    private final EmailService emailService;

    private final UsernameChecker usernameChecker;
    private final EmailChecker emailChecker;

    @Override
    public User create(UserPayload userPayload) {
        log.info("Creating a user: {}", userPayload);

        usernameChecker.check(userPayload.getUserName());
        emailChecker.check(userPayload.getEmail());

        User user = UserMapper.mapUserFromUserCreationPayload(userPayload);
        user.setPassword(passwordEncoder.encode(userPayload.getPassword()));
        user.setOrganization(organizationService.getOrganization());

        return userRepository.save(user);
    }

    @Override
    public void assignTaskToUsers(Task task, List<Long> userIds) {
        log.info("Assign task to users: {}", userIds);

        List<User> users = userRepository.findAllById(userIds);
        for (User user : users) {
            user.getTasks().add(task);
            userRepository.save(user);
        }

        log.info("Task assigned to users.");

        this.notifyUsersAboutNewTask(users, task);
    }

    @Override
    public List<UserDTO> getUsersOfOrganization() {
        log.info("Getting users of organization");
        return userRepository.findAllByOrganizationAndRoleIs(
                organizationService.getOrganization(),
                Role.ROLE_USER)
                .stream()
                .map(UserDTOMapper::mapFromUser)
                .collect(Collectors.toList());
    }

    private void notifyUsersAboutNewTask(List<User> users, Task task) {
        for (User user : users) {
            log.info("Sending email to user: {}", user.getEmail());
            EmailMessage emailMessage = EmailMessageMapper.createForNotifyingNewTask(user.getEmail(), task);
            emailService.send(emailMessage);
        }
    }
}

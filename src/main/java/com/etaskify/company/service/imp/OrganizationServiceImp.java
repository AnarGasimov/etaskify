package com.etaskify.company.service.imp;

import com.etaskify.company.model.Organization;
import com.etaskify.company.model.User;
import com.etaskify.company.repository.OrganizationRepository;
import com.etaskify.company.security.service.UserPrincipalService;
import com.etaskify.company.service.OrganizationService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;


@Service
@AllArgsConstructor
@Slf4j
public class OrganizationServiceImp implements OrganizationService {

    private final OrganizationRepository organizationRepository;
    private final UserPrincipalService userPrincipalService;


    @Override
    public Organization create(Organization organization) {

        System.out.println(organization);
        log.info("Creating organization: {}", organization);
        return organizationRepository.save(organization);
    }

    @Override
    public Organization getOrganization() {
        log.info("Getting organization");
        User user = userPrincipalService.getUser();
        return user.getOrganization();
    }

}

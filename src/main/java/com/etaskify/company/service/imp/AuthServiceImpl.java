package com.etaskify.company.service.imp;


import com.etaskify.company.mapper.UserMapper;
import com.etaskify.company.model.Organization;
import com.etaskify.company.model.User;
import com.etaskify.company.payload.LoginPayload;
import com.etaskify.company.payload.RegistrationFormPayload;
import com.etaskify.company.repository.UserRepository;
import com.etaskify.company.security.JwtTokenProvider;
import com.etaskify.company.service.AuthService;
import com.etaskify.company.service.OrganizationService;
import com.etaskify.company.tool.EmailChecker;
import com.etaskify.company.tool.UsernameChecker;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
@Slf4j
@AllArgsConstructor
public class AuthServiceImpl implements AuthService {

    private final UserRepository userRepository;
    private final OrganizationService organizationService;
    private final JwtTokenProvider tokenProvider;
    private final AuthenticationManager authenticationManager;
    private final UsernameChecker usernameChecker;
    private final EmailChecker emailChecker;
    private final PasswordEncoder passwordEncoder;


    @Override
    public void register(RegistrationFormPayload payload) {

        emailChecker.check(payload.getEmail());
        usernameChecker.check(payload.getUserName());

        Organization organization = organizationService.create(new Organization(payload.getOrganization(),payload.getPhoneNumber(),payload.getAddress()));

        User user = UserMapper.mapUserFromRegistrationPayload(payload);
        user.setOrganization(organization);
        user.setPassword(passwordEncoder.encode(payload.getPassword()));

        System.out.println("user " + user);
        userRepository.save(user);
    }

    @Override
    public String login(LoginPayload payload) {
        log.info("Sign in by username or email: {}", payload.getUsernameOrEmail());
        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        payload.getUsernameOrEmail(),
                        payload.getPassword()
                )
        );

        SecurityContextHolder.getContext().setAuthentication(authentication);

        return tokenProvider.generateToken(authentication);
    }
}

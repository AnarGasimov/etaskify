package com.etaskify.company.service.imp;


import com.etaskify.company.dto.TaskDTO;
import com.etaskify.company.mapper.TaskDTOMapper;
import com.etaskify.company.mapper.TaskMapper;
import com.etaskify.company.model.Task;
import com.etaskify.company.model.User;
import com.etaskify.company.payload.TaskPayload;
import com.etaskify.company.repository.TaskRepository;
import com.etaskify.company.security.service.UserPrincipalService;
import com.etaskify.company.service.OrganizationService;
import com.etaskify.company.service.TaskService;
import com.etaskify.company.service.UserService;
import com.etaskify.company.tool.UsersChecker;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@Slf4j
@AllArgsConstructor
public class TaskServiceImpl implements TaskService {

    private final TaskRepository taskRepository;
    private final UserService userService;
    private final UserPrincipalService userPrincipalService;
    private final OrganizationService organizationService;
    private final UsersChecker usersChecker;



    @Override
    public TaskDTO create(TaskPayload taskPayload) {


        usersChecker.check(taskPayload.getUserIds());

        Task task = TaskMapper.mapFromPayload(taskPayload);
        task.setOrganization(organizationService.getOrganization());
        taskRepository.save(task);

        log.info("Created task: {}", taskPayload);

        userService.assignTaskToUsers(task, taskPayload.getUserIds());

        return TaskDTOMapper.mapFromTask(task);
    }

    @Override
    public List<TaskDTO> getMyTasks() {
        log.info("Getting tasks");
        User user = userPrincipalService.getUser();
        return user.getTasks().stream()
                .map(TaskDTOMapper::mapFromTask)
                .collect(Collectors.toList());
    }

    @Override
    public List<TaskDTO> getOrganizationTasks() {

        return taskRepository.findAllByOrganization(organizationService.getOrganization())
                .stream()
                .map(TaskDTOMapper::mapFromTask)
                .collect(Collectors.toList());
    }
}

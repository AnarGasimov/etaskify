package com.etaskify.company.service;

import com.etaskify.company.model.Organization;

public interface OrganizationService {
    Organization create(Organization organization);
    Organization getOrganization();

}

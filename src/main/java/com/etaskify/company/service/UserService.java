package com.etaskify.company.service;



import com.etaskify.company.dto.UserDTO;
import com.etaskify.company.model.Task;
import com.etaskify.company.model.User;
import com.etaskify.company.payload.UserPayload;


import java.util.List;


public interface UserService {
    User create(UserPayload userPayload);
    void assignTaskToUsers(Task task, List<Long> userIds);
    List<UserDTO> getUsersOfOrganization();
}
package com.etaskify.company.exception;

import org.springframework.http.HttpStatus;

public class BadRequestException extends BaseControllerException {

    public BadRequestException(String message) {
        super(HttpStatus.BAD_REQUEST, message);
    }
}

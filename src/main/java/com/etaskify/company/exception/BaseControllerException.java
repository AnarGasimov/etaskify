package com.etaskify.company.exception;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.http.HttpStatus;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class BaseControllerException extends RuntimeException {
    private HttpStatus httpStatus;
    private String message;
}

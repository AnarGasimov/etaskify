package com.etaskify.company.exception;

import org.springframework.http.HttpStatus;

public class UserNotFoundException extends BaseControllerException {

    public UserNotFoundException(String message) {
        super(HttpStatus.NOT_FOUND, message);
    }

}
